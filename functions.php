<?php

/**
 *    Widgets
 */
if (!function_exists('habits_setup')) {
    add_action( 'after_setup_theme', 'habits_setup' );
    function habits_setup() {
        add_theme_support('post_thumbnail', array('post', 'page'));
    }
}

function habits_enqueue_styles() {

    $parent_style = 'parent-style';

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}
add_action( 'wp_enqueue_scripts', 'habits_enqueue_styles' );

/**
 *    oEmbeds
 */
wp_embed_register_handler( 'eventick', '#(http[s]):\/\/((www\.|)eventick\.com\.br)(\/e\/)([\w\-\.]+)#i', 'wp_embed_handler_eventick' );

function wp_embed_handler_eventick( $matches, $attr, $url, $rawattr ) {
        
	$embed = sprintf(
	        '<div>
	            <style type="text/css">.ev-embed{width: 100%%; border: 0;}</style>
	            <script src="https://s3.amazonaws.com/embed-evt/iframeResizer.min.js"></script>
	            <iframe class="ev-embed" src="https://www.eventick.com.br/e/%1$s" scrolling="no"></iframe>
	            Alternativamente, você pode adquirir seu ticket diretamente na <a href="https://www.eventick.com.br/%1$s" target="_blank">página do Eventick</a>
	            <script>iFrameResize({checkOrigin: false, heightCalculationMethod: "documentElementScroll"});</script>
	        </div>',
			esc_attr($matches[5])
			);

	return apply_filters( 'embed_eventick', $embed, $matches, $attr, $url, $rawattr );
}